// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';

var $ = require('jquery');

$(function() {
  $("#scroll-top-button").click( function() {
  	$('html,body').animate({scrollTop: 0}, 250);
  });
});
